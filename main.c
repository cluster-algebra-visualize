/*
 * Copyright (c) 2016, S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <errno.h>
#include <locale.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <curses.h>

uint_fast8_t enough_colors = 0;

/* what the program is doing */
typedef enum show {
        /* */
        SH_NORMAL, SH_NAMES
} show;

/* strings for arrow bodies */
char *dirstr[] = {
        /* */
        [(-1 + 1) * 3 + (-1 + 1)] = "/", /* */
        [(-1 + 1) * 3 + (0 + 1)] = "|",  /* */
        [(-1 + 1) * 3 + (1 + 1)] = "\\", /* */
        [(0 + 1) * 3 + (-1 + 1)] = "-",  /* */
        [(0 + 1) * 3 + (0 + 1)] = "o",   /* */
        [(0 + 1) * 3 + (1 + 1)] = "-",   /* */
        [(1 + 1) * 3 + (-1 + 1)] = "\\", /* */
        [(1 + 1) * 3 + (0 + 1)] = "|",   /* */
        [(1 + 1) * 3 + (1 + 1)] = "/",   /* */
};

/* strings for arrow heads */
char *arrstr[] = {
        /* */
        [(-1 + 1) * 3 + (-1 + 1)] = "\u2b69", /* */
        [(-1 + 1) * 3 + (0 + 1)] = "v",       /* */
        [(-1 + 1) * 3 + (1 + 1)] = "\u2b68",  /* */
        [(0 + 1) * 3 + (-1 + 1)] = "<",       /* */
        [(0 + 1) * 3 + (0 + 1)] = "\u2b59",   /* */
        [(0 + 1) * 3 + (1 + 1)] = ">",        /* */
        [(1 + 1) * 3 + (-1 + 1)] = "\u2b66",  /* */
        [(1 + 1) * 3 + (0 + 1)] = "^",        /* */
        [(1 + 1) * 3 + (1 + 1)] = "\u2b67",   /* */
};

/* an arrow */
typedef struct arrow {
        /* index of the point from */
        size_t from;

        /* index of the point to */
        size_t to;

        /* multiplicity of the arrow */
        int16_t multiplicity;
} arrow;

/* x-y coords */
typedef struct coord {
        /* */
        int x;

        /* */
        int y;
} coord;

/* A cluster algebra */
typedef struct ca {
        /* contains leftmost and bottommost */
        coord bl;

        /* contains rightmost and topmost */
        coord ur;

        /* how many points */
        size_t point_num;

        /* x-coords of each point */
        coord *point_coord;

        /* how many arrows */
        size_t arrow_num;

        /* arrow data */
        arrow *arrow;
} ca;

/* given x and y, return the index of the point (possibly created) */
static int
find_make_point(int x,
                int y,
                size_t *out_idx,
                ca *data)
{
        size_t i;
        coord *c;
        void *newmem;

        for (i = 0; i < data->point_num; ++i) {
                c = &data->point_coord[i];

                if (c->x == x &&
                    c->y == y) {
                        if (out_idx) {
                                *out_idx = i;
                        }

                        return 0;
                }
        }

        data->point_num++;

        if (!(newmem = realloc(data->point_coord, data->point_num *
                               sizeof *data->point_coord))) {
                data->point_num--;

                return ENOMEM;
        }

        data->point_coord = newmem;
        data->point_coord[i] = (coord) { .x = x, .y = y };

        if (out_idx) {
                *out_idx = i;
        }

        return 0;
}

/* add an arrow p1 -> p2 in data */
static int
add_arrow(size_t p1,
          size_t p2,
          int mult,
          ca *data)
{
        size_t i;
        arrow *a;
        void *newmem;

        if (p1 >= data->point_num ||
            p2 >= data->point_num ||
            p1 == p2) {
                return EINVAL;
        }

        for (i = 0; i < data->arrow_num; ++i) {
                a = &data->arrow[i];

                if (a->from == p1 &&
                    a->to == p2) {
                        a->multiplicity += mult;

                        return 0;
                } else if (a->from == p2 &&
                           a->to == p1) {
                        a->multiplicity -= mult;

                        return 0;
                }
        }

        data->arrow_num++;

        if (!(newmem = realloc(data->arrow, data->arrow_num *
                               sizeof *data->arrow))) {
                data->arrow_num--;

                return ENOMEM;
        }

        data->arrow = newmem;
        data->arrow[data->arrow_num - 1] = (arrow) { .from = p1, .to = p2,
                                                     .multiplicity = mult };

        return 0;
}

/* fix up the bl and ur data */
static void
recalc_extents(ca *data)
{
        size_t i;

        data->bl = data->point_coord[0];
        data->ur = data->point_coord[0];

        for (i = 0; i < data->point_num; ++i) {
                coord *c = &data->point_coord[i];

                if (data->bl.x > c->x) {
                        data->bl.x = c->x;
                }

                if (data->bl.y > c->y) {
                        data->bl.y = c->y;
                }

                if (data->ur.x < c->x) {
                        data->ur.x = c->x;
                }

                if (data->ur.y < c->y) {
                        data->ur.y = c->y;
                }
        }
}

/* read a file descriptor, initialize from it */
static int
init_from_file(FILE *f,
               ca *data)
{
        /*
         * each line should be in form
         *
         *     (x,y)
         * or
         *     (x,y) -> (x,y)
         *
         * all referenced points exist, referenced arrows affect
         * multiplicity.
         */
        char *line = 0;
        size_t n = 0;
        ssize_t err = 0;

        while (getline(&line, &n, f) >= 0) {
                long x1 = 0;
                long y1 = 0;
                size_t p1 = 0;
                long x2 = 0;
                long y2 = 0;
                size_t p2 = 0;
                char *s;

                if (!(s = strchr(line, '('))) {
                        continue;
                }

                errno = 0;
                x1 = strtol(++s, 0, 10);

                if (errno) {
                        continue;
                }

                if (!(s = strchr(s, ','))) {
                        continue;
                }

                y1 = strtol(++s, 0, 10);

                if (errno) {
                        continue;
                }

                if ((err = find_make_point(x1, y1, &p1, data))) {
                        goto done;
                }

                if (!(s = strstr(s, "->"))) {
                        continue;
                }

                if (!(s = strchr(s, '('))) {
                        continue;
                }

                errno = 0;
                x2 = strtol(++s, 0, 10);

                if (errno) {
                        continue;
                }

                if (!(s = strchr(s, ','))) {
                        continue;
                }

                y2 = strtol(++s, 0, 10);

                if (errno) {
                        continue;
                }

                if ((err = find_make_point(x2, y2, &p2, data))) {
                        goto done;
                }

                if ((err = add_arrow(p1, p2, 1, data))) {
                        goto done;
                }
        }

        if (errno != EINVAL &&
            !err) {
                err = errno;
        }

        if (data->point_num == 0) {
                goto done;
        }

        recalc_extents(data);
done:
        free(line);

        return (int) err;
}

/* draw the algebra */
static void
draw_ca(ca *data,
        show s,
        int width,
        int height,
        int cx,
        int cy)
{
        size_t i;

        /* draw the lines */
        for (i = 0; i < data->arrow_num; ++i) {
                arrow *a = &data->arrow[i];
                coord cur = { 0 };
                coord target = { 0 };
                int xdelta_orig;
                int ydelta_orig;
                int cpair = 0;

                if (!a->multiplicity ||
                    a->from == a->to) {
                        continue;
                }

                cur = data->point_coord[(a->multiplicity > 0) ? a->from :
                                        a->to];
                target = data->point_coord[(a->multiplicity > 0) ? a->to :
                                           a->from];
                xdelta_orig = target.x - cur.x;
                ydelta_orig = target.y - cur.y;
                cpair = (int) (M_PI / 8.0 + 4.0 * M_1_PI * atan2(
                                       (double) ydelta_orig,
                                       (double) xdelta_orig) + 8);
                cpair = ((cpair + 8) % 8) + 1;

                while (cur.x != target.x ||
                       cur.y != target.y) {
                        int xdelta_cur = target.x - cur.x;
                        int ydelta_cur = target.y - cur.y;
                        int xdir = (xdelta_cur > 0) - (xdelta_cur < 0);
                        int ydir = (ydelta_cur > 0) - (ydelta_cur < 0);

                        if (xdelta_orig == 0 ||
                            xdelta_cur == 0) {
                                xdir = 0;
                        } else if (ydelta_orig == 0 ||
                                   ydelta_cur == 0) {
                                ydir = 0;
                        } else if ((xdelta_orig / ydelta_orig != xdelta_cur /
                                    ydelta_cur) ||
                                   (ydelta_orig / xdelta_orig != ydelta_cur /
                                    xdelta_cur)) {
                                xdir *= !!(xdelta_cur / ydelta_cur);
                                ydir *= !!(ydelta_cur / xdelta_cur);
                        }

                        if (height - cur.y + cy - (height / 2) >= 0 &&
                            -cur.y + cy - (height / 2) < -1 &&
                            cur.x - cx + (width / 2) < width &&
                            cur.x - cx + (width / 2) >= 0) {
                                move(height - cur.y + cy - (height / 2), cur.x -
                                     cx + (width / 2));

                                if (a->multiplicity > 1) {
                                        attron(A_STANDOUT);
                                }

                                if (enough_colors) {
                                        attron(COLOR_PAIR(cpair));
                                }

                                if (cur.x + xdir == target.x &&
                                    cur.y + ydir == target.y) {
                                        addstr(arrstr[(ydir + 1) * 3 + (xdir +
                                                                        1)]);
                                } else {
                                        addstr(dirstr[(ydir + 1) * 3 + (xdir +
                                                                        1)]);
                                }

                                standend();
                        }

                        cur.x += xdir;
                        cur.y += ydir;
                }
        }

        /* now draw the points */
        for (i = 0; i < data->point_num; ++i) {
                coord *c = &data->point_coord[i];
                int x = c->x - cx + (width / 2);
                int y = c->y - cy + (height / 2);

                if (x < 0 ||
                    x >= width ||
                    y <= 0 ||
                    y >= height) {
                        continue;
                }

                move(height - y, x);

                if (s == SH_NORMAL) {
                        addstr("\u2022");
                } else {
                        printw("%x", i);
                }
        }
}

/* prompt for a point to mutate at, mutate there */
int
mutate_at(ca *data,
          unsigned width,
          unsigned height)
{
        char input[80];
        char *s = input;
        char *t;
        size_t i;
        size_t j;
        long l;
        size_t p;
        int err;

        memset(&input, 0, sizeof input);
        move(height - 1, 0);

        for (i = 0; i < width; ++i) {
                addch(' ');
        }

        move(height - 1, 0);
        addstr("Mutate at (lists ok): ");
        curs_set(1);
        echo();
        getnstr(input, (sizeof input) - 1);
        noecho();
        curs_set(0);

        while (*s) {
                errno = 0;
                l = strtoll(s, &t, 16);

                if (errno ||
                    s == t ||
                    l < 0 ||
                    (size_t) l >= data->point_num) {
                        goto advance;
                }

                p = (size_t) l;

                /* add all needed new arrows */
                for (i = 0; i < data->arrow_num; ++i) {
                        arrow *a = &data->arrow[i];

                        for (j = i + 1; j < data->arrow_num; ++j) {
                                arrow *b = &data->arrow[j];

                                if (a->to == p &&
                                    b->from == p) {
                                        if ((err = add_arrow(a->from, b->to,
                                                             a->multiplicity *
                                                             b->multiplicity,
                                                             data))) {
                                                return err;
                                        }
                                } else if (a->from == p &&
                                           b->to == p) {
                                        if ((err = add_arrow(b->from, a->to,
                                                             a->multiplicity *
                                                             b->multiplicity,
                                                             data))) {
                                                return err;
                                        }
                                }
                        }
                }

                /* reverse all arrows */
                for (i = 0; i < data->arrow_num; ++i) {
                        arrow *a = &data->arrow[i];

                        if (a->from == p) {
                                a->from = a->to;
                                a->to = p;
                        } else if (a->to == p) {
                                a->to = a->from;
                                a->from = p;
                        }
                }

advance:
                s = (s == t) ? (s + 1) : t;
        }

        return 0;
}

/* prompt for a point to delete, delete it */
int
del_point(ca *data,
          unsigned width,
          unsigned height)
{
        char input[80];
        char *s = input;
        char *t;
        size_t i;
        long l;
        size_t p;

        memset(&input, 0, sizeof input);
        move(height - 1, 0);

        for (i = 0; i < width; ++i) {
                addch(' ');
        }

        move(height - 1, 0);
        addstr("Delete point(s) (lists ok): ");
        curs_set(1);
        echo();
        getnstr(input, (sizeof input) - 1);
        noecho();
        curs_set(0);

        while (*s) {
                errno = 0;
                l = strtoll(s, &t, 16);

                if (errno ||
                    s == t ||
                    l < 0 ||
                    (size_t) l >= data->point_num) {
                        goto advance;
                }

                p = (size_t) l;

                /* kill arrows and swap with other */
                for (i = 0; i < data->arrow_num; ++i) {
                        arrow *a = &data->arrow[i];

                        if (a->from == p ||
                            a->to == p) {
                                *a = (arrow) { 0 };
                        } else if (a->from == data->point_num - 1) {
                                a->from = p;
                        } else if (a->to == data->point_num - 1) {
                                a->to = p;
                        }
                }

                data->point_coord[p] = data->point_coord[data->point_num - 1];
                data->point_num--;
advance:
                s = (s == t) ? (s + 1) : t;
        }

        return 0;
}

/* prompt for a point to insert, insert it */
int
inp_point(ca *data,
          int width,
          int height,
          int cx,
          int cy)
{
        int i;
        int orig_x = (width) / 2;
        int orig_y = (height) / 2;
        int x = orig_x;
        int y = orig_y;
        int c;

        move(height - 1, 0);

        for (i = 0; i < width; ++i) {
                addch(' ');
        }

        move(height - 1, 0);
        addstr("h/j/k/l to move, Return to add");
        curs_set(1);

        do {
                if (x < 0) {
                        x = 0;
                } else if (x >= width) {
                        x = width - 1;
                }

                if (y < 0) {
                        y = 0;
                } else if (y >= height) {
                        y = height - 1;
                }

                move(height - y, x);
                refresh();

                switch (c = getch()) {
                case 'h':
                case KEY_LEFT:
                        x--;
                        break;
                case 'l':
                case KEY_RIGHT:
                        x++;
                        break;
                case 'j':
                case KEY_DOWN:
                        y--;
                        break;
                case 'k':
                case KEY_UP:
                        y++;
                        break;
                case 'y':
                case KEY_A1:
                        x--;
                        y++;
                        break;
                case 'u':
                case KEY_A3:
                        x++;
                        y++;
                        break;
                case 'b':
                case KEY_C1:
                        x--;
                        y--;
                        break;
                case 'n':
                case KEY_C3:
                        x++;
                        y--;
                        break;
                case 'H':
                        x -= 8;
                        break;
                case 'L':
                        x += 8;
                        break;
                case 'J':
                        y -= 8;
                        break;
                case 'K':
                        y += 8;
                        break;
                case 'Y':
                        x -= 8;
                        y += 8;
                        break;
                case 'U':
                        x += 8;
                        y += 8;
                        break;
                case 'B':
                        x -= 8;
                        y -= 8;
                        break;
                case 'N':
                        x += 8;
                        y -= 8;
                        break;
                }
        } while (c != ' ' &&
                 c != '\n' &&
                 c != '\r' &&
                 c != KEY_ENTER);

        curs_set(0);

        return find_make_point(x - orig_x + cx, y - orig_y + cy, 0, data);
}

/* prompt for two points to add an arrow between, add it */
int
inp_arrow(ca *data,
          int width,
          int height)
{
        char input[80];
        char *s = input;
        char *t;
        int i;
        long l;
        size_t p1;
        size_t p2;

        /* from */
        memset(&input, 0, sizeof input);
        move(height - 1, 0);

        for (i = 0; i < width; ++i) {
                addch(' ');
        }

        move(height - 1, 0);
        addstr("From: ");
        curs_set(1);
        echo();
        getnstr(input, (sizeof input) - 1);
        noecho();
        curs_set(0);
        errno = 0;
        l = strtoll(s, &t, 16);

        if (errno ||
            s == t ||
            l < 0 ||
            (size_t) l >= data->point_num) {
                return 0;
        }

        p1 = (size_t) l;

        /* to */
        memset(&input, 0, sizeof input);
        move(height - 1, 0);

        for (i = 0; i < width; ++i) {
                addch(' ');
        }

        move(height - 1, 0);
        addstr("To: ");
        curs_set(1);
        echo();
        getnstr(input, (sizeof input) - 1);
        noecho();
        curs_set(0);
        errno = 0;
        l = strtoll(s, &t, 16);

        if (errno ||
            s == t ||
            l < 0 ||
            (size_t) l >= data->point_num) {
                return 0;
        }

        p2 = (size_t) l;

        /* mult */
        memset(&input, 0, sizeof input);
        move(height - 1, 0);

        for (i = 0; i < width; ++i) {
                addch(' ');
        }

        move(height - 1, 0);
        addstr("Multiplicity: ");
        curs_set(1);
        echo();
        getnstr(input, (sizeof input) - 1);
        noecho();
        curs_set(0);
        errno = 0;

        if (!*input) {
                l = 1;
        } else {
                l = strtoll(s, &t, 10);

                if (errno ||
                    s == t) {
                        return 0;
                }
        }

        return add_arrow(p1, p2, l, data);
}

/* prompt for a file to save state to, save it */
int
save_state(ca *data,
           unsigned width,
           unsigned height)
{
        char input[4096];
        size_t i;
        int m;
        FILE *f;

        memset(&input, 0, sizeof input);
        move(height - 1, 0);

        for (i = 0; i < width; ++i) {
                addch(' ');
        }

        move(height - 1, 0);
        addstr("Save to file: ");
        curs_set(1);
        echo();
        getnstr(input, (sizeof input) - 1);
        noecho();
        curs_set(0);

        if (!*input) {
                return 0;
        }

        if (!(f = fopen(input, "w"))) {
                move(height - 1, 0);

                for (i = 0; i < width; ++i) {
                        addch(' ');
                }

                move(height - 1, 0);
                addstr("Could not write to file!");
                attron(A_STANDOUT);
                addstr("[OK]");
                standend();
                refresh();
                getch();

                return 0;
        }

        for (i = 0; i < data->point_num; ++i) {
                coord *c = &data->point_coord[i];
                fprintf(f, "(%d,%d)\n", c->x, c->y);
        }

        fprintf(f, "\n");

        for (i = 0; i < data->arrow_num; ++i) {
                arrow *a = &data->arrow[i];
                coord *p1 = &data->point_coord[a->from];
                coord *p2 = &data->point_coord[a->to];

                for (m = 0; m < a->multiplicity; ++m) {
                        fprintf(f, "(%d,%d) -> (%d,%d)\n", p1->x, p1->y, p2->x,
                                p2->y);
                }

                for (m = 0; m > a->multiplicity; --m) {
                        fprintf(f, "(%d,%d) -> (%d,%d)\n", p2->x, p2->y, p1->x,
                                p1->y);
                }
        }

        fclose(f);
        move(height - 1, 0);

        for (i = 0; i < width; ++i) {
                addch(' ');
        }

        move(height - 1, 0);
        addstr("Written  ");
        attron(A_STANDOUT);
        addstr("[OK]");
        standend();
        refresh();
        getch();

        return 0;
}

/* read keys, do stuff */
static int
displayloop(ca *data)
{
        int width = -1;
        int height = -1;
        uint_fast8_t should_quit = 0;
        int c = 0;
        int cx = data->bl.x + (data->ur.x - data->bl.x + 1) / 2;
        int cy = data->bl.y + (data->ur.y - data->bl.y + 1) / 2;
        int i;
        int err;

        do {
                getmaxyx(stdscr, height, width);
                height = (height >= 0) ? height : 0;
                width = (width >= 0) ? width : 0;
                clear();
                draw_ca(data, SH_NORMAL, width, height, cx, cy);
                move(height - 1, 0);
                refresh();
                addch('M' | A_STANDOUT);
                addstr("utate at    ");
                addstr("New ");
                addch('p' | A_STANDOUT);
                addstr("oint    ");
                addstr("New ");
                addch('a' | A_STANDOUT);
                addstr("rrow    ");
                addch('D' | A_STANDOUT);
                addstr("elete point    ");
                addch('S' | A_STANDOUT);
                addstr("ave state    ");
                addch('Q' | A_STANDOUT);
                addstr("uit");

                for (i = 74; i < width; ++i) {
                        addch(' ');
                }

                switch ((c = getch())) {
                case KEY_EXIT:
                case 'Q':
                case 'q':
                        should_quit = 1;
                        break;
                case 'M':
                case 'm':
                        draw_ca(data, SH_NAMES, width, height, cx, cy);
                        refresh();

                        if ((err = mutate_at(data, width, height))) {
                                return err;
                        }

                        break;
                case 'D':
                case 'd':
                        draw_ca(data, SH_NAMES, width, height, cx, cy);
                        refresh();

                        if ((err = del_point(data, width, height))) {
                                return err;
                        }

                        break;
                case 'P':
                case 'p':

                        if ((err = inp_point(data, width, height, cx, cy))) {
                                return err;
                        }

                        break;
                case 'A':
                case 'a':
                        draw_ca(data, SH_NAMES, width, height, cx, cy);
                        refresh();

                        if ((err = inp_arrow(data, width, height))) {
                                return err;
                        }

                        break;
                case 'S':
                case 's':
                        save_state(data, width, height);
                        break;
                case 'h':
                        cx--;
                        break;
                case 'l':
                        cx++;
                        break;
                case 'j':
                        cy--;
                        break;
                case 'k':
                        cy++;
                        break;
                case KEY_RESIZE:
                default:
                        break;
                }
        } while (!should_quit);

        return 0;
}

/* main */
int
main(int argc,
     const char **argv)
{
        FILE *template = 0;
        ca data = { 0 };
        int err = 0;

        setlocale(LC_ALL, "");

        if (argc == 2) {
                if (!(template = fopen(argv[1], "r"))) {
                        perror("Could not open template file");

                        return ENOENT;
                }

                err = init_from_file(template, &data);
                fclose(template);

                if (err) {
                        goto done;
                }
        } else if (argc != 1) {
                fprintf(stderr, "Usage: %s [path-to-input-file]\n", argv[0]);

                return E2BIG;
        }

        if (!initscr()) {
                return EINVAL;
        }

        if (has_colors()) {
                start_color();
        }

        if (COLORS >= 256) {
                enough_colors = 1;
                use_default_colors();

                /* no special reasons for these, I don't know color */
                init_pair(1, 1, -1);
                init_pair(2, 3, -1);
                init_pair(3, 2, -1);
                init_pair(4, 5, -1);
                init_pair(5, 4, -1);
                init_pair(6, 58, -1);
                init_pair(7, 202, -1);
                init_pair(8, 6, -1);
        }

        cbreak();
        noecho();
        curs_set(0);
        clear();
        nonl();
        intrflush(stdscr, FALSE);
        keypad(stdscr, TRUE);
        err = displayloop(&data);
        endwin();
done:

        if (err) {
                errno = err;
                perror(0);
        }

        return err;
}
