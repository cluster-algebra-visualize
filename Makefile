PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
SHAREDIR ?= $(PREFIX)/share
MANDIR ?= $(PREFIX)/share/man

# Compiler options
CC=gcc
LD=gcc
CFLAGS=-std=c99 -D_POSIX_C_SOURCE=200809L
LDFLAGS=-lm

CFLAGS+=$(shell pkg-config --cflags ncursesw)
LDFLAGS+=$(shell pkg-config --libs ncursesw)

PROG=cluster-algebra-visualize

CFILES=$(wildcard *.c)
OFILES=$(patsubst %.c,%.o,$(CFILES))

default: all
.PHONY: all

all: $(PROG)

.PHONY: clean
clean:
	rm -f $(OFILES)
	rm -f $(PROG)

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f cluster-algebra-visualize $(DESTDIR)$(BINDIR)
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -f cluster-algebra-visualize.1 \
	$(DESTDIR)$(MANDIR)/man1/cluster-algebra-visualize.1
	mkdir -p $(DESTDIR)$(SHAREDIR)/cluster-algebra-visualize
	cp -f 7_a.txt $(DESTDIR)$(SHAREDIR)/cluster-algebra-visualize/7_a.txt
	cp -f 7_b.txt $(DESTDIR)$(SHAREDIR)/cluster-algebra-visualize/7_b.txt

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f cluster-algebra-visualize
	cd $(DESTDIR)$(MANDIR)/man1 && rm -f cluster-algebra-visualize.1
	cd $(DESTDIR)$(SHAREDIR)/cluster-algebra-visualize && rm -f 7_a.txt 7_b.txt

# Not automagically kept in sync!

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

$(PROG): $(OFILES)
	$(LD) $(LDFLAGS) -o $@ $^
